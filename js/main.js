(function () {
  const labels = [
    "Round 1",
    "Round 2",
    "Round 3",
    "Round 4",
    "Round 5",
    "Round 6",
  ];
  const data = {
    labels: labels,
    datasets: [
      {
        label: "Stock Price",
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgb(255, 99, 132)",
        data: [0, 50, 20, 30, 40, 50],
      },
    ],
  };

  const config = {
    type: "line",
    data,
    options: {},
  };
  let myChart = new Chart(document.getElementById("myChart"), config);

  let clickButton = 0;
  function upbetMoney() {
    let up = document.getElementById("up");
    let money = document.getElementById("money");
    let betMoney = document.getElementById("betMoney");
    if (money.textContent > 0) {
      let lastbetMoney = parseFloat(betMoney.textContent) + 2;
      betMoney.textContent = lastbetMoney;
      let afterbetMoney = parseFloat(money.textContent) - 2;
      money.textContent = afterbetMoney;
      clickButton = 1;
    }
  }

  function downbetMoney() {
    let down = document.getElementById("down");
    let money = document.getElementById("money");
    let betMoney = document.getElementById("betMoney");
    if (money.textContent > 0) {
      let lastbetMoney = parseFloat(betMoney.textContent) + 2;
      betMoney.textContent = lastbetMoney;
      let afterbetMoney = parseFloat(money.textContent) - 2;
      money.textContent = afterbetMoney;
      clickButton = 0;
    }
  }

  function generateResult() {
    let lastEle = data.datasets[0].data.length;
    let outcome = data.datasets[0].data[lastEle - 1];
    let outcome2 = Math.floor(Math.random() * 200);
    let lastRound = "Round " + (labels.length + 1);
    let money = document.getElementById("money");
    let betMoney = document.getElementById("betMoney");
    let newPrice = document.getElementById("newPrice");
    if (outcome2 > outcome && clickButton == 1) {
      let addMoney =
        parseFloat(money.textContent) +
        parseFloat(betMoney.textContent) +
        parseFloat(betMoney.textContent);
      money.textContent = addMoney;
      betMoney.textContent = 0;
      newPrice.textContent = outcome2;
      data.datasets[0].data.push(outcome2);
      labels.push(lastRound);
      updateScales(myChart);
    } else if (outcome > outcome2 && clickButton == 0) {
      let addMoney2 =
        parseFloat(money.textContent) +
        parseFloat(betMoney.textContent) +
        parseFloat(betMoney.textContent);
      money.textContent = addMoney2;
      betMoney.textContent = 0;
      newPrice.textContent = outcome2;
      data.datasets[0].data.push(outcome2);
      labels.push(lastRound);
      updateScales(myChart);
    } else {
      betMoney.textContent = 0;
      newPrice.textContent = outcome2;
      data.datasets[0].data.push(outcome2);
      labels.push(lastRound);
      updateScales(myChart);
    }
  }
  function updateScales(chart) {
    var xScale = chart.scales.x;
    var yScale = chart.scales.y;
    chart.options.scales = {
      newId: {
        display: true,
      },
      y: {
        display: true,
      },
    };
    chart.update();
    xScale = chart.scales.newId;
    yScale = chart.scales.y;
  }

  function timeClock(second, callback) {
    setTimeout(function () {
      callback();
      timeClock(second, callback);
    }, second * 1000);
  }

  function init() {
    timeClock(1, function () {
      let seconds = document.getElementById("seconds");
      if (seconds.textContent > 0) {
        seconds.textContent -= 1;
      } else if (seconds.textContent == 0) {
        generateResult();
        seconds.textContent = 4;
      }
    });
  }

  window.upbetMoney = upbetMoney;
  window.downbetMoney = downbetMoney;
  window.init = init;
})();
